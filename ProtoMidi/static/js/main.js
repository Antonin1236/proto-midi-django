
        $(document).ready(function() {
            $("#addPlaylistFrom").submit(function(e) {

                e.preventDefault(); // avoid to execute the actual submit of the form.

                var form = $(this);
                var url = form.attr('action');
                var formData = new FormData($(this)[0])

                $.ajax({
                       type: "POST",
                       url: url,
                        mimeType:'application/json',
                        dataType:'json',
                        data: formData,
                        contentType: false,
                        processData: false,
                       success: function(data)

                       {

                            $('nav').append(
                                $('<a/>').addClass('nav-link nav-link-playlist grey-200 d-flex pb-0').attr('href', '#')
                                .attr('playlist_id', data['playlist_id']).html(
                                    $('#addPlaylistFrom [name="title"]').val()
                                )
                            )

                            $('#addPlaylistFrom [name="title"]').val('');
                           $('#addPlaylistFrom [name="description"]').val('');
                           $('#addPlaylistFrom [name="image"]').val('');
                           $('#addPlaylist').modal('hide');

                       }
                });


            });



        });

         $(document).on('click', "a.nav-link-home", function() {
            loadHomePage();
         });


         function showHistory(page){
             pageIsLoading();
             content_div = $('<div/>').addClass('p-5');
             content_div.append($('<h1/>').addClass('text-light display-3').html('Historique'))

                $.ajax({
                       type: "GET",
                       url: "/rest-api/HistoryListViewSet/?format=json&page="+page,
                       success: function(data)
                       {


                            list = $('<ul/>').addClass('list-group text-light mt-2 mb-2');
                            $.each(data['results'], async function( index, value ) {
                                 var d = new Date(value['date']);

                                    var curr_day = d.getDate();
                                    var curr_month = d.getMonth();
                                    var curr_year = d.getFullYear();

                                    var curr_hour = d.getHours();
                                    var curr_min = d.getMinutes();
                                    var curr_sec = d.getSeconds();

                                list.append(
                                    $('<li/>').addClass('list-group-item d-flex align-items-center').attr('style', 'background-color: hsla(0,0%,100%,.1)').append([
                                        $('<img/>').attr('src', '/media/'+value['music']['album_image']).attr('style', 'width: 4rem; height: 4rem;'),
                                        $('<p/>').html(value['music']['title']).addClass('h2 ml-3'),
                                        $('<p/>').html('écouté le ' + curr_day + '/' + curr_month + '/'+ curr_year + ' à ' + curr_hour + 'h' + curr_min).addClass('h2 ml-3')

                                    ])
                                )

                            });




                            pagination = $('<nav/>');

                            ul_pagination = $('<ul/>').addClass('pagination');

                            if(data['page'] <= 1){
                                ul_pagination.append(
                                    $('<li/>').addClass('page-item disabled').append(
                                        $('<a/>').addClass('page-link')
                                            .attr('href', '#')
                                            .attr('tabindex','-1')
                                            .html('Previous')
                                    )
                                )
                            }else{
                                 ul_pagination.append(
                                    $('<li/>').addClass('page-item').append(
                                        $('<a/>').addClass('page-link')
                                            .attr('href', '#')
                                            .attr('onclick', 'showHistory('+ Number(Number(data['page']) - 1) +')')
                                            .html('Previous')
                                    )
                                )
                            }
                            ul_pagination.append(
                                    $('<li/>').addClass('page-item active').append(
                                        $('<a/>').addClass('page-link')
                                            .attr('href', '#')
                                            .attr('onclick', 'showHistory('+ data['page'] +')')
                                            .html(data['page'])
                                    )
                                )

                            if((data['page'])*data['page_size']+1 > data['total']){
                                ul_pagination.append(
                                    $('<li/>').addClass('page-item disabled').append(
                                        $('<a/>').addClass('page-link')
                                            .attr('href', '#')
                                            .attr('tabindex','-1')
                                            .html('Next')
                                    )
                                )
                            }else{
                                 ul_pagination.append(
                                    $('<li/>').addClass('page-item').append(
                                        $('<a/>').addClass('page-link')
                                            .attr('href', '#')
                                            .attr('onclick', 'showHistory('+ Number(Number(data['page']) + 1) +')')
                                            .html('Next')
                                    )
                                )
                            }

                             pagination.append(ul_pagination)


                            content_div.append(list)
                             content_div.append(pagination)
                             placeIsMainPage(content_div);


                       }
                });



         }
         function addMusicToHistory(music_id){
                $.ajax({
                       type: "GET",
                       url: "/rest-api/addMusicToHistory/"+music_id,
                       success: function(data)
                       {}
                });
         }
         $(document).on('click', "a.nav-link-history", function() {
                $('.active').removeClass('active');
                $(this).addClass( "active" );
                 showHistory(1);
                //placeIsMainPage('<p> void </p>');

         });
         $(document).on('click', "a.nav-link-research", function() {
                $('.active').removeClass('active');
                $(this).addClass( "active" );

                pageIsLoading();


                search_bar = $('<div/>').addClass('p-3').append(
                    $('<form/>').attr('id', 'search-form').append(
                        $('<div/>').addClass('input-group has-validation').append([
                            $('<input/>').attr('type', 'text').attr('name', 'search').attr('style', 'background-color: hsla(0,0%,100%,.1)').attr('required', 'required').addClass('form-control text-light'),
                            $('<div/>').addClass('input-group-prepend').append(
                                $('<button/>').attr('type', 'submit').addClass('input-group-text text-dark').append('<i class="fa fa-search"></i>')
                            )
                        ])

                    )
                ).append($('<div/>').attr('id', 'search-box'))





                placeIsMainPage(search_bar);

         });
         $(document).on("submit", "#search-form", function(event)
            {
                event.preventDefault();
                 var form = $(this);

                $.ajax({
                       type: "get",
                       url: '/rest-api/music/',
                       data: form.serialize(), // serializes the form's elements.
                       success: function(data)
                       {
                            list = $('<ul/>').addClass('list-group text-light mt-2 mb-2');
                            $.each(data, async function( index, value ) {

                                if(value['is_fav']){
                                     fav_b = $('<button/>')
                                                   .addClass('border-0 text-white bg-transparent')
                                                     .attr('onclick', 'addMusicToFavList(this)')
                                                     .attr('music_id', value['id'])
                                                  .append($('<i/>').addClass('fa fa-2x  fa-heart'));
                                }else{
                                     fav_b = $('<button/>')
                                                  .addClass('border-0 text-white bg-transparent')
                                                  .attr('onclick', 'addMusicToFavList(this)')
                                                  .attr('music_id', value['id'])
                                                  .append($('<i/>').addClass('fa fa-2x fa-heart-o'));
                                }
                                list_b = $('<div/>').addClass('d-inline-block no-arrow').append([
                                            $('<a/>')
                                                  .addClass('dropdown-toggle border-0 text-white bg-transparent')
                                                  .attr('href', '#')
                                                   .attr('role', 'button')
                                                   .attr('data-toggle', 'dropdown')
                                                   .attr('aria-haspopup', 'true')
                                                   .attr('aria-expanded', 'false')
                                                  .attr('onclick', 'setPlaylistList(this)')
                                                  .attr('music_id', value['id'])
                                                  .append($('<i/>').addClass('fa fa-2x fa-folder')),

                                            $('<div/>').addClass('dropdown-menu dropdown-menu-left shadow animated--grow-in')
                                                       .attr('style', 'max-height: 12rem; overflow-y: auto;')
                                                       .attr('aria-labelledby','userDropdown')
                                                       .append($('<div/>').addClass('text-center').append($('<i/>').addClass('fa fa-spinner fa-spin')))

                                      ]);
                                list.append(
                                    $('<li/>').addClass('list-group-item d-flex align-items-center justify-content-between').attr('style', 'background-color: hsla(0,0%,100%,.1)').append([
                                        $('<div/>').append([
                                            fav_b,
                                            list_b

                                        ]),
                                        $('<img/>').attr('src', '/media/'+value['album_image']).attr('style', 'width: 4rem; height: 4rem;'),
                                        $('<p/>').html(value['title']).addClass('h2 ml-3'),
                                        $('<p/>').html(value['album_title']).addClass('h2 ml-3'),
                                        $('<p/>').html('de '+value['author']).addClass('h2 ml-3'),

                                        $('<button/>').addClass('btn btn-success btn-circle btn-lg')
                                                  .attr('onclick', 'playOneMusicButton(this)')
                                                  .attr('m_name', value['title'])
                                                  .attr('m_artist', value['author'])
                                                  .attr('m_album', value['album_title'])
                                                  .attr('m_url', value['songFile'])
                                                  .attr('m_id', value['id'])
                                                  .attr('m_cover_art_url', '/media/'+value['album_image'])
                                                  .append($('<i/>').addClass('fa fa-play'))

                                    ])
                                )

                            });
                             $('#search-box').html('')
                             if (data.length > 0){
                              $('#search-box').append(list)
                             }else{
                              $('#search-box').html('<p>Aucuns résultats</p>')
                             }


                       }
                 });

            });


         $(document).on('click', "a.nav-link-playlist-fav", function() {
                $('.active').removeClass('active');
                $(this).addClass( "active" );
                pageIsLoading();

                $.ajax({
                       type: "GET",
                       url: "/rest-api/musicList-fav/?format=json",
                       success: function(data)
                       {

                            printPlaylist(data);

                       }
                });
         });

         function printMusicLine(musicId){

             return new Promise((resolve) => {
             $.ajax({
                       type: "GET",
                       url: '/rest-api/MusicGetViewSet/'+musicId,
                       success: function(data)
                       {
                            if(data['is_fav']){
                                 fav_b = $('<button/>')
                                               .addClass('border-0 text-white bg-transparent')
                                                 .attr('onclick', 'addMusicToFavList(this)')
                                                 .attr('music_id', data['id'])
                                              .append($('<i/>').addClass('fa fa-2x  fa-heart'));
                            }else{
                                 fav_b = $('<button/>')
                                              .addClass('border-0 text-white bg-transparent')
                                              .attr('onclick', 'addMusicToFavList(this)')
                                              .attr('music_id', data['id'])
                                              .append($('<i/>').addClass('fa fa-2x fa-heart-o'));
                            }
                            list_b = $('<div/>').addClass('d-inline-block no-arrow').append([
                                        $('<a/>')
                                              .addClass('dropdown-toggle border-0 text-white bg-transparent')
                                              .attr('href', '#')
                                               .attr('role', 'button')
                                               .attr('data-toggle', 'dropdown')
                                               .attr('aria-haspopup', 'true')
                                               .attr('aria-expanded', 'false')
                                              .attr('onclick', 'setPlaylistList(this)')
                                              .attr('music_id', data['id'])
                                              .append($('<i/>').addClass('fa fa-2x fa-folder')),

                                        $('<div/>').addClass('dropdown-menu dropdown-menu-left shadow animated--grow-in')
                                                   .attr('style', 'max-height: 12rem; overflow-y: auto;')
                                                   .attr('aria-labelledby','userDropdown')
                                                   .append($('<div/>').addClass('text-center').append($('<i/>').addClass('fa fa-spinner fa-spin')))

                                  ]);




                            resolve($('<tr/>').addClass('').append([
                                $('<td/>').append(fav_b).append(list_b),
                                $('<td/>').attr('style','width:2.4rem').append($('<img/>').attr('src','/media/'+data['album_image']).attr('style','height:2.3rem;width:2.3rem')),
                                $('<td/>').html(data['title']),
                                $('<td/>').html(data['author']),
                                $('<td/>').html(data['album_title']),
                                $('<td/>').append($('<button/>')
                                              .addClass('btn btn-success btn-circle btn-lg')
                                              .attr('onclick', 'playOneMusicButton(this)')
                                              .attr('m_name', data['title'])
                                              .attr('m_artist', data['author'])
                                              .attr('m_album', data['album_title'])
                                              .attr('m_url', data['songFile'])
                                              .attr('m_id', data['id'])
                                              .attr('m_cover_art_url', '/media/'+data['album_image'])
                                              .append($('<i/>').addClass('fa fa-play'))),
                            ]));



                       }
                });
            });

         }

         function setPlaylistList(a_){

              var container = $(a_).next();
              $.ajax({
                       type: "GET",
                       url: 'rest-api/musicPlayList/?format=json',
                       dataType:'json',
                       success: function(data)
                       {
                            container.empty();
                            container.append('<p class="pt-2  pl-4 pr-4 small"> Ajouter / supprimer la musique d\'une playliste</p>');
                              container.append('<div class="dropdown-divider"></div>');
                            $.each(data, async function( index, value ) {
                                if( value['musics'].includes(Number ($(a_).attr('music_id') ))){
                                   container.append($('<a/>').addClass('dropdown-item text-danger')
                                        .attr('href', '#')
                                        .attr('music_id', $(a_).attr('music_id'))
                                        .attr('onclick','addMusicToList(this)')
                                        .attr('list_id', value['id'])
                                        .append('<i class="fa fa-minus-square"></i> ')
                                        .append(value['title']));
                                }else{
                                    container.append($('<a/>').addClass('dropdown-item')
                                        .attr('href', '#')
                                        .attr('music_id', $(a_).attr('music_id'))
                                        .attr('onclick','addMusicToList(this)')
                                        .attr('list_id', value['id'])
                                        .append('<i class="fa fa-plus-square"></i> ')
                                        .append(value['title']));
                                }

                            });
                       }
              });



         }
         function loadHomePage(){
                $('.active').removeClass('active');
                $('a.nav-link-home').addClass( "active" );
                pageIsLoading();

                $.ajax({
                       type: "GET",
                       url: '/rest-api/category/?format=json',
                       dataType:'json',
                       success: function(data)
                       {

                           content_div = $('<div/>').addClass('p-4 text-white');

                            $.each(data, async function( index, value ) {
                                if (value['albums'].length > 0){

                                    album_list = $('<div/>').addClass('d-flex justify-content-begin overflow-auto');
                                     $.each(value['albums'], async function( index_album, value_album ) {

                                           album_list.append(
                                                $('<a/>').attr('href','#').attr('album_api_link', value_album['url']).addClass('album-link mr-3').append($('<div/>').addClass('shadow-lg p-2 ').attr('style', 'background-color : hsla(0,0%,100%,.1); border-radius: 5px;width: 10em').append([
                                                    $('<img/>').attr('src', value_album['image']).attr('style', 'width: 100%'),
                                                    $('<h5/>').html(value_album['title']).addClass('mt-1'),
                                                ]))
                                           )
                                     });


                                    content_div.append(
                                        $('<section/>').append([
                                          $('<h2/>').html(value['name']),
                                          album_list,
                                          $('<hr/>'),
                                        ])
                                    );
                                }
                            });

                           placeIsMainPage(content_div);

                       }
                });


         }


         function addMusicFromFavListAjax(btn){
                $.ajax({
                       type: "GET",
                       url: '/rest-api/addMusicFavListView/'+$(btn).attr('music_id')+'?format=json',
                       success: function(data)
                       {

                           Toaster('Ajouter à vos favoris', 'success');

                       }
                });

         }
         function removeMusicFromFavListAjax(btn){
                $.ajax({
                       type: "GET",
                       url: '/rest-api/removeMusicFavListView/'+$(btn).attr('music_id')+'?format=json',
                       success: function(data)
                       {
                              if($('.list-type').first().html() == 'favlist'){
                                   $(btn).closest('tr' ).remove();
                              }
                              Toaster('Retirer de vos favoris', 'danger');

                       }
                });

         }

         function addMusicFromListAjax(btn){
                $.ajax({
                       type: "GET",
                       url: '/rest-api/addMusicToListView/'+$(btn).attr('music_id')+'/'+$(btn).attr('list_id')+'?format=json',
                       success: function(data)
                       {

                           Toaster('Musique ajouté à la playlist avec succès', 'success');

                       }
                });

         }
         function removeMusicFromListAjax(btn){
                $.ajax({
                       type: "GET",
                       url: '/rest-api/removeMusicToListView/'+$(btn).attr('music_id')+'/'+$(btn).attr('list_id')+'?format=json',
                       success: function(data)
                       {
                              if($('.list-type').first().html() == 'favlist'){
                                   $(btn).closest('tr' ).remove();
                              }
                              Toaster('Musique retiré de la playlist avec succès', 'danger');

                       }
                });

         }
        function addMusicToList(btn){
            if ($(btn).children().first().hasClass('fa-minus-square')){

                 removeMusicFromListAjax(btn);

            }else{

                addMusicFromListAjax(btn);
            }
         }

         function addMusicToFavList(btn){
            if ($(btn).children().first().hasClass('fa-heart')){
                $(btn).children().first().removeClass('fa-heart');
                $(btn).children().first().addClass('fa-heart-o');
                 removeMusicFromFavListAjax(btn);

            }else{
                $(btn).children().first().removeClass('fa-heart-o');
                $(btn).children().first().addClass('fa-heart');
                addMusicFromFavListAjax(btn);
            }
         }
         function playOneMusicButton(btn){
            if(Amplitude.getPlayerState() !='playing' || (Amplitude.getPlayerState() =='playing' && Amplitude.getActiveSongMetadata().name != $(btn).attr('m_name') ) ){
                /*if(Amplitude.getPlayerState() =='playing'){Amplitude.stop()}*/

            }
            s = {
                    "name": $(btn).attr('m_name'),
                    "artist": $(btn).attr('m_artist'),
                    "album": $(btn).attr('m_album'),
                    "url": $(btn).attr('m_url'),
                    "cover_art_url": $(btn).attr('m_cover_art_url'),
             }
             console.log(s);
             Amplitude.addSong(s);
             Amplitude.bindNewElements();
             Amplitude.playSongAtIndex(Amplitude.getSongs().length -1);
             Amplitude.play();
             addMusicToHistory($(btn).attr('m_id'))
            //Amplitude.playSongAtIndex(0)

         }
         function printPlaylist(data){
                 tbody = $('<tbody/>');

                 $.each(data['musics'], async function( index, value ) {

                      tbody.append(await printMusicLine(value));
                    });
                  musics = $('<div/>').addClass('m-5').append($('<table/>').addClass('table table-dark bg-transparent table-borderless')
                    .append($('<thead/>').append(
                        [$('<th/>').html('Ajouter'),

                        $('<th/>').attr('style','width:2.4rem').html(''),
                        $('<th/>').html('Titre'),
                         $('<th/>').html('Auteur'),
                         $('<th/>').html('Album'),
                          $('<th/>'),]
                        )
                        ).append(tbody));

                   content_list = [
                             $('<div/>').attr('class', 'm-3 row').append(
                                [
                                    $('<div/>').attr('class', 'col-auto').append(
                                        $('<img/>').attr('class', 'shadow').attr('src',data['image']).attr('style', 'width : 20rem; height : 20rem;')
                                    ),
                                    $('<div/>').attr('class', 'col-sm text-white').append([
                                            $('<p/>').attr('class', 'text-uppercase list-type').html(data['type']),
                                            $('<h2/>').attr('class', 'display-3').html(data['title']),
                                            $('<p/>').attr('class', '').html(data['description']),
                                        ]
                                    )
                                ]

                             ),musics
                             ]
                     if(data['type'] == "favlist" || data['type'] == "playlist" ){
                         content_list.push( $('<a/>').addClass('ml-5 btn nav-link-home btn-light').html('Ajouter d\'autre musiques') );
                     }
                 placeIsMainPage(content_list);
         }
        $(document).on('click', "a.nav-link-playlist", function() {
                $('.active').removeClass('active');
                $(this).addClass( "active" );
                pageIsLoading();
                $.ajax({
                       type: "GET",
                       url: '/rest-api/musicList/'+$(this).attr('playlist_id')+'/?format=json',
                       success: function(data)
                       {

                            printPlaylist(data);

                       }
                });

         });
         $(document).on('click', "a.album-link", function() {

                pageIsLoading();
                $.ajax({
                       type: "GET",
                       url: $(this).attr('album_api_link'),
                       success: function(data)
                       {

                            printPlaylist(data);

                       }
                });

         });

         $( document ).ready(function() {
            loadHomePage();
        });

