from django.contrib.auth.models import User, Group
from rest_framework import serializers

from App.models import MusicList, Music, Category,Album, History


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups', 'password']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class AlbumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Album
        fields = ['url', 'title', 'description', 'image', 'categories', 'musics']
        xtra_kwargs = {'categories': {'required': False}}


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    albums = AlbumSerializer(many=True, read_only=True)
    class Meta:
        model = Category
        fields = ['url', 'name', 'albums']
        xtra_kwargs = {'albums': {'required': False}}


class MusicListSerializer(serializers.ModelSerializer):
    class Meta:
        model = MusicList
        fields = ['id', 'url', 'title', 'description', 'type', 'image', 'user', 'musics']




class MusicSerializer(serializers.ModelSerializer):
    album_title = serializers.ReadOnlyField()
    album_image = serializers.ReadOnlyField()

    class Meta:
        model = Music
        fields = ['url', 'id', 'title', 'author', 'songFile', 'minuteDuration', 'album_title', 'album_image']




class HistorySerializer(serializers.ModelSerializer):
    music = MusicSerializer(many=False)

    class Meta:
        model = History
        fields = ['id', 'music', 'user', 'date']

