"""ProtoMidi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from . import views, settings

router = routers.DefaultRouter()
router.register(r'musicList', views.MusicListViewSet)
router.register(r'music', views.MusicViewSet)
router.register(r'category', views.CategoryViewSet)
router.register(r'album', views.AlbumViewSet)
router.register(r'history', views.HistoryViewSet)

urlpatterns = [
    path('', include('App.urls')),
    path('admin/', admin.site.urls),

    path('accounts/', include('django.contrib.auth.urls')),
    path('rest-api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('rest-api/musicList-fav/', views.MusicListFavViewSet, name="musicList_fav"),
    path('rest-api/musicPlayList/', views.MusicPlayListViewSet, name="musicPlayList"),
    path('rest-api/MusicGetViewSet/<int:music_id>', views.MusicGetViewSet, name="MusicGetViewSet"),

    path('rest-api/addMusicFavListView/<int:music_id>', views.addMusicFavListView, name="addMusicFavListView"),
    path('rest-api/removeMusicFavListView/<int:music_id>', views.removeMusicFavListView, name="removeMusicFavListView"),

    path('rest-api/addMusicToListView/<int:music_id>/<int:list_id>', views.addMusicToListView, name="addMusicToListView"),
    path('rest-api/removeMusicToListView/<int:music_id>/<int:list_id>', views.removeMusicToListView, name="removeMusicToListView"),

    path('rest-api/HistoryListViewSet/', views.HistoryListViewSet, name="HistoryListViewSet"),

    path('rest-api/addMusicToHistory/<int:music_id>', views.addMusicToHistory,
         name="addMusicToHistory"),




]

urlpatterns +=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
